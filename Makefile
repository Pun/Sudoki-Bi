##
## Makefile for sudoku-bi in /home/sam_t/Workstation/sudoki-bi
## 
## Made by sam_t
## Login   <sam_t@epitech.net>
## 
## Started on  Fri Feb 28 20:36:31 2014 sam_t
## Last update Sun Mar 27 16:04:35 2014 kettan_s
##

CC	=	cc

RM	=	rm -f

SRCS	=	src/main.c \
		src/grid.c \
		src/display.c \
		src/backtracking.c \
		src/check.c \
		src/my/my_putchar.c \
		src/my/my_putstr.c \
		src/my/my_strlen.c \
		src/my/my_put_nbr.c \
		src/my/my_putstrerror.c

OBJS	=	$(SRCS:.c=.o)

NAME	=	bin/sudoki-bi

CFLAGS  +=	-W -Wall -O3 -s -ansi -pedantic

all: $(NAME)

$(NAME): $(OBJS)
	 $(CC) -o $(NAME) $(OBJS)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
