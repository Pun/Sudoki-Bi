/*
** check.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi/sources
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 13:13:51 2014 sam_t
** Last update Sun Mar  2 13:40:40 2014 sam_t
*/

#include "../includes/sudoku.h"

int	check_line(int **grid, int nb, int line)
{
  int	i;

  i = 0;
  while (i < MAX)
    {
      if (grid[line][i] == nb)
	return (FALSE);
      ++i;
    }
  return (TRUE);
}

int	check_column(int **grid, int nb, int column)
{	
  int	i;

  i = 0;
  while (i < MAX)
    {
      if (grid[i][column] == nb)
	return (FALSE);
      ++i;
    }
  return (TRUE);
}

int	check_block(int **grid, int nb, int line, int column)
{
  int	i;
  int	j;
  int	x;
  int	y;

  j = 3 * (line / 3);
  y = j + 2;
  x = (3 * (column / 3)) + 2;
  while (j <= y)
    {
      i = 3 * (column / 3);
      while (i <= x)
	{
	  if (grid[j][i] == nb)
	    return (FALSE);
	  ++i;
	}
      ++j;
    }
  return (TRUE);
}


int	check(int **grid, int nb, int line, int column)
{
  if (check_line(grid, nb, line) == FALSE)
    return (FALSE);
  if (check_column(grid, nb, column) == FALSE)
    return (FALSE);
  if (check_block(grid, nb, line, column) == FALSE)
    return (FALSE);
  return (TRUE);
}

int	check_final(int **grid)
{
  int	x;
  int	y;

  y = MAX - 1;
  while (y >= 0)
    {
      x = MAX - 1;
      while (x >= 0)
	{
	  if (grid[y][x] == VOID)
	    return (FALSE);
	  --x;
	}
      --y;
    }
  return (TRUE);
}
