/*
** my_putchar.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 11:23:47 2014 sam_t
** Last update Sun Mar  2 13:46:39 2014 sam_t
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}
