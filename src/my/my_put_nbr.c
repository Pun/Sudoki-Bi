/*
** my_put_nbr.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 11:24:16 2014 sam_t
** Last update Sun Mar  2 13:47:21 2014 sam_t
*/

#include "../../includes/my.h"

void	my_put_nbr(int nb)
{
  int	i;
  char	c;

  if (nb < 0)
    {
      my_putchar('-');
      nb = nb * -1;
    }
  i = nb;
  if (nb > 9)
    {
      nb = nb / 10;
      my_put_nbr(nb);
      nb = i % 10;
      c = '0' + nb;
      my_putchar(c);
    }
  else
    {
      c = '0' + i;
      my_putchar(c);
    }
}
