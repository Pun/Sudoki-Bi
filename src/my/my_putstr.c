/*
** my_putstr.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi/sources/my
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 11:27:53 2014 sam_t
** Last update Sun Mar  2 13:36:51 2014 sam_t
*/

#include "../../includes/my.h"

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_putchar(str[i]);
      i++;
    }
}
