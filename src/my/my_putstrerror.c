/*
** my_puterror.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi/sources/my
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 13:19:58 2014 sam_t
** Last update Sun Mar  2 13:51:10 2014 sam_t
*/

#include "../../includes/my.h"
#include <unistd.h>

void	my_puterror(char c)
{
  write(2, &c, 1);
}

void	my_putstrerror(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_puterror(str[i]);
      ++i;
    }
}
