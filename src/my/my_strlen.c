/*
** my_strlen.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi/sources/my
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 13:19:30 2014 sam_t
** Last update Sun Mar  2 13:19:50 2014 sam_t
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    ++i;
  return (i);
}
