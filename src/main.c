/*
** main.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi/sources
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 13:45:23 2014 sam_t
** Last update Sun Mar  2 14:21:22 2014 sam_t
*/

#include "../includes/my.h"
#include "../includes/sudoku.h"
#include <stdlib.h>

int	sudoku()
{
  int	**grid;

  while ((grid = init_grid()) != NULL)
    {
      backtracking(grid);
      display(grid);
      free_grid(grid);
    }
  return (0);
}

int	main()
{
  sudoku();
  return (0);
}
