/*
** backtracking.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sat Mar  1 18:09:25 2014 sam_t
** Last update Sun Mar  2 14:31:34 2014 sam_t
*/

#include "../includes/sudoku.h"

int	backtracking_body(int **grid, int column, int line)
{
  int	nbr;
  int	tmp_nbr;

  if (grid[line][column] == 0)
    {
      nbr = 1;
      while (nbr <= 9)
	{
	  if (check(grid, nbr, line, column) == TRUE)
	    {
	      tmp_nbr = grid[line][column];
	      grid[line][column] = nbr;
	      backtracking(grid);
	      if (check_final(grid) == TRUE)
		return (SKIP);
	      grid[line][column] = tmp_nbr;
	    }
	  ++nbr;
	}
      return (SKIP);
    }
  return (TRUE);
}

int	backtracking(int **grid)
{
  int	line;
  int	column;

  line = 0;
  while (line < 9)
    {
      column = 0;
      while (column < 9)
	{
	  if (backtracking_body(grid, column, line) == SKIP)
	    return (SKIP);
	  ++column;
	}
      ++line;
    }
  return (TRUE);
}
