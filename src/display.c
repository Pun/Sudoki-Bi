/*
** display.c for sudoku-bi in /home/sam_t/Workstation/sudoku-bi
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 11:23:18 2014 sam_t
** Last update Sun Mar  2 13:40:34 2014 sam_t
*/

#include "../includes/my.h"
#include "../includes/sudoku.h"

void		display(int **grid)
{
  static int	i = 0;
  int		x;
  int		y;

  y = 0;
  if (i)
    my_putstr("####################\n");
  my_putstr("|------------------|\n");
  while (y < MAX)
    {
      x = 0;
      my_putchar('|');
      while (x < MAX)
  	{
	  my_putchar(' ');
	  my_put_nbr(grid[y][x]);
  	  ++x;
  	}
      my_putstr("|\n");
      ++y;
    }
  my_putstr("|------------------|\n");
  i = 1;
}
