/*
** init_grid.c for sudoku-bi in /home/sam_t/Workstation/sudoki-bi
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Fri Feb 28 21:08:18 2014 sam_t
** Last update Sun Mar  2 14:15:35 2014 sam_t
*/

#include "../includes/my.h"
#include "../includes/sudoku.h"
#include <stdlib.h>
#include <unistd.h>

void	free_grid(int **grid)
{
  int	i;

  i = 0;
  while (i < MAX)
    {
      free(grid[i]);
      ++i;
    }
  free(grid);
}

int	**malloc_grid()
{
  int	**grid;
  int	i;

  if ((grid = malloc(sizeof(int *) * MAX)) == NULL)
    return (NULL);
  i = 0;
  while (i < MAX)
    {
      if ((grid[i] = malloc(sizeof(int) * MAX)) == NULL)
	return (NULL);
      ++i;
    }
  return (grid);
}

char	*read_grid()
{
  char	*buffer;
  int	len;

  if ((buffer = malloc(sizeof(char) * 232)) == NULL)
    {
      my_putstrerror("Error : malloc failed\n");
      return (NULL);
    }
  if ((len = read(0, buffer, 231)) <= 0)
    return (NULL);
  buffer[len] = '\0';
  return (buffer);
}



void	fill_grid(char *buffer, int **grid)
{
  int	i;
  int	j;
  int	k;

  i = 23;
  j = 0;
  k = 0;
  while (i < 208)
    {
      if (buffer[i] >= '0' && buffer[i] <= '9')
	grid[j][k] = buffer[i] - '0';
      else
	grid[j][k] = VOID;
      if (k == 8)
	{
	  k = 0;
	  ++j;
	  i += 5;
	}
      else
	{
	  ++k;
	  i += 2;
	}
    }
}

int	**init_grid()
{
  int	**grid;
  char	*buffer;

  if ((grid = malloc_grid()) == NULL)
    {
      my_putstrerror("Error : malloc failed\n");
      return (NULL);
    }
  if ((buffer = read_grid()) == NULL)
    {
      free_grid(grid);
      free(buffer);
      return (NULL);
    }
  fill_grid(buffer, grid);
  free(buffer);
  return (grid);
}
