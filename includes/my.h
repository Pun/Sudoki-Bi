/*
** my.h for sudoku-bi in /home/sam_t/Workstation/sudoku-bi/includes
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 12:46:17 2014 sam_t
** Last update Sun Mar  2 13:51:41 2014 sam_t
*/

#ifndef MY_H_
# define MY_H_

void	my_putchar(char c);
void	my_putstr(char *str);
void	my_put_nbr(int nb);
void	my_putstrerror(char *str);
void	my_strlen(char *str);

#endif	/* !MY_H_ */
