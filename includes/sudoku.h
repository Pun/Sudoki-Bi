/*
** sudoku.h for sudoku in /home/sam_t/Workstation/sudoku-bi/includes
** 
** Made by sam_t
** Login   <sam_t@epitech.net>
** 
** Started on  Sun Mar  2 12:49:02 2014 sam_t
** Last update Sun Mar  2 13:48:14 2014 sam_t
*/

#ifndef SUDOKU_H_
# define SUDOKU_H_

# define TRUE (1)
# define FALSE (0)
# define VOID (0)
# define SKIP (0)
# define MAX (9)

int	**init_grid();
void	free_grid(int **grid);
void	display(int **grid);
int	backtracking(int **grid);
int	check(int **grid, int nb, int line, int column);
int	check_final(int **grid);

#endif	/* !SUDOKU_H_ */
